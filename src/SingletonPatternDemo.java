/**
 * Created by sanket on 7/18/2016.
 */
public class SingletonPatternDemo {

    public static void main(String[] args) {
        SingletonObject singletonObject = SingletonObject.getInstance();
        singletonObject.showMessage();
    }

}
